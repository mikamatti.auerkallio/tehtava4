const mysql = require("mysql");

//ADD HEADERS


//DB Connection
const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "puhelinluettelo",
  multipleStatements: true, //out parametria varten aliohjelmassa
});

//Connect to DB
con.connect((err) => {
  if (err) {
    console.log("Error connecting to Db");
    return;
  }
  console.log("Connection established");
});

const express = require("express");
const bodyParser = require("body-parser");
const app = express().use(bodyParser.json());

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader("Access-Control-Allow-Origin", "*");
  
    // Request methods you wish to allow
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );
  
    // Request headers you wish to allow
    res.setHeader(
      "Access-Control-Allow-Headers",
      "X-Requested-With,content-type"
    );
  
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader("Access-Control-Allow-Credentials", true);
  
    // Pass to next layer of middleware
    next();
});

//CREATE, READ, EDIT, DELETE functions
// GET all users
app.get("/users", (req, res) => {
    con.query('SELECT * FROM henkilot', function (error, results) {
        if (error) throw error;
        //return res.send({ error: false, data: results, message: 'users list.' });
        res.json(results);
    });
});

// GET a user
app.get("/users/:id", (req, res) => {
    const id = Number(req.params.id);
    con.query('SELECT * FROM henkilot WHERE id = ' + id, function (error, results) {
        if (error) throw error;
        res.json(results ? results : { message: "Not found" });
    });
});

// ADD a user
app.post("/add", (req, res) => {
    const henkilo = req.body; // esimerkkimuoto henkilo = { nimi: 'Ankka Roope', puhelin: '050-1231232' };
    console.log(henkilo);
    if (!henkilo) {
        return res.status(400).send({ error:true, message: 'Please provide user' });
    }

    con.query('INSERT INTO henkilot SET ?', henkilo, function (error, results) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'New user has been created successfully.' });
    });
});

// UPDATE a user info
app.put("/users/:id", (req, res) => {
    const id = Number(req.params.id);
    const updatedUser = req.body;
   
    if(updatedUser.nimi != null && updatedUser.puhelin != null) {
        //Jos kumpikaan ei ole null, päivitetään molemmat
        con.query('UPDATE henkilot SET nimi = ?, puhelin = ? Where ID = ?', [updatedUser.nimi, updatedUser.puhelin, req.params.id],
        function(error, results) {
            if (error) throw error;
            return res.send({ error: false, data: results, message: 'Name and number updated.' });
        });
    } else if(updatedUser.nimi != null) {
        //Päivitetään vain nimi
        con.query('UPDATE henkilot SET nimi = ? Where ID = ?', [updatedUser.nimi, req.params.id],
        function(error, results) {
            if (error) throw error;
            return res.send({ error: false, data: results, message: 'Name only updated.' });
        });
    } else if(updatedUser.puhelin != null) {
        //Päivitetään vain puhelin
        con.query('UPDATE henkilot SET puhelin = ? Where ID = ?', [updatedUser.puhelin, req.params.id],
        function(error, results) {
            if (error) throw error;
            return res.send({ error: false, data: results, message: 'Number only updated.' });
        });
    }     
});

// DELETE a user
app.delete("/users/:id", (req, res) => {
    con.query('DELETE FROM henkilot Where ID = ?', [req.params.id],
    function(error, results) {
            if (error) throw error;
            return res.send({ error: false, data: results, message: 'User deleted.' });
    });
});

app.listen(3000, () => {
    console.log("Server listening at port 3000");
});



// con.query("SELECT * FROM henkilot", (err, rows) => {
//     if (err) throw err;

//     console.log("Data received from Db:");
//     rows.forEach((row) => {
//     console.log(`${row.nimi}, puhelin on ${row.puhelin}`);
//     });
// });

// const henkilo = { nimi: 'Ankka Roope', puhelin: '050-1231232' };
// con.query('INSERT INTO henkilot SET ?', henkilo, (err, res) => {
//   if(err) throw err;

//   console.log('Last insert ID:', res.insertId);
// });

// con.query(
//     'UPDATE henkilot SET puhelin = ? Where ID = ?',
//     ['044-6544655', 3],
//     (err, result) => {
//       if (err) throw err;

//       console.log(`Changed ${result.changedRows} row(s)`);
//     }
//   );

// con.query("DELETE FROM henkilot WHERE id = ?", [5], (err, result) => {
//   if (err) throw err;

//   console.log(`Deleted ${result.affectedRows} row(s)`);
// });

// con.query("CALL sp_get_henkilot()", function (err, rows) {
//   if (err) throw err;

//   rows[0].forEach( (row) => {
//     console.log(`${row.nimi},  puhelin: ${row.puhelin}`);
//   });
//   console.log(rows);
// });

// con.query("CALL sp_get_henkilon_tiedot(1)", (err, rows) => {
//   if (err) throw err;

//   console.log("Data received from Db:\n");
//   console.log(rows[0]);
// });


//Tehtava tee aliohjelma sp_get_henkilon_puhelinnumero
// con.query("CALL sp_get_henkilon_puhelinnumero('Ankka Aku')", (err, rows) => {
//    if (err) throw err;

//    console.log("Data received from Db:\n");
//    console.log(`puhelin: ${rows[0][0].puhelin}`);
// });


// con.query(
//     "SET @henkilo_id = 0; CALL sp_insert_henkilo(@henkilo_id, 'Matti Miettinen', '044-5431232'); SELECT @henkilo_id",
//     (err, rows) => {
//       if (err) throw err;

//       console.log('Data received from Db:\n');
//       console.log(rows);
//     }
//   );
// const userSubmittedVariable =



//   "1"; /*ettÃ¤ kukaan ei voi syÃ¶ttÃ¤Ã¤ tÃ¤tÃ¤:
// const userSubmittedVariable = '1; DROP TABLE henkilot';
// con.query(
//   `SELECT * FROM henkilot WHERE id = ${mysql.escape(userSubmittedVariable)}`,
//   (err, rows) => {
//     if (err) throw err;
//     console.log(rows);
//   }
// );

// con.end((err) => {
//   // The connection is terminated gracefully
//   // Ensures all remaining queries are executed
//   // Then sends a quit packet to the MySQL server.
// });
