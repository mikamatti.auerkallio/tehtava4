DELIMITER $$
CREATE PROCEDURE `sp_get_henkilot`()
BEGIN
	SELECT id, nimi, puhelin FROM henkilot;
END $$
GRANT EXECUTE ON PROCEDURE puhelinluettelo.sp_get_henkilot TO 'root'@'localhost' 
IDENTIFIED BY 'root'

DELIMITER $$
CREATE PROCEDURE `sp_get_henkilon_tiedot`(
	IN henkilo_id INT
)
BEGIN
	SELECT nimi, puhelin FROM henkilot WHERE id = henkilo_id;
END $$

GRANT EXECUTE ON PROCEDURE puhelinluettelo.sp_get_henkilon_tiedot TO 'root'@'localhost' 
IDENTIFIED BY 'root'

DELIMITER $$
CREATE PROCEDURE `sp_get_henkilon_puhelinnumero`(
	IN henkilo_nimi VARCHAR(50)
)
BEGIN
	SELECT puhelin FROM henkilot WHERE nimi = henkilo_nimi;
END $$

GRANT EXECUTE ON PROCEDURE puhelinluettelo.sp_get_henkilon_puhelinnumero TO 'root'@'localhost' 
IDENTIFIED BY 'root'

DELIMITER $$
CREATE PROCEDURE `sp_insert_henkilo`(
	OUT henkilo_id INT,
	IN henkilo_nimi VARCHAR(25),
	IN henkilo_puhelin VARCHAR(25)
)
BEGIN 
	INSERT INTO henkilot(nimi, puhelin)
	VALUES(henkilo_nimi, henkilo_puhelin);
	SET henkilo_id = LAST_INSERT_ID();
END $$

GRANT EXECUTE ON PROCEDURE puhelinluettelo.sp_insert_henkilo TO 'root'@'localhost' 
IDENTIFIED BY 'root'