CREATE DATABASE IF NOT EXISTS puhelinluettelo;

USE puhelinluettelo;

CREATE TABLE IF NOT EXISTS henkilot (
	id INT(11) NOT NULL AUTO_INCREMENT,
	nimi VARCHAR(50),
	puhelin VARCHAR(50),
	PRIMARY KEY(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

INSERT INTO henkilot (id, nimi, puhelin) 
VALUES(1, 'Ankka Aku', '040-2342342'),
(2, 'Hopo Hessu', '044-2342343'),
(3, 'Naamio Musta', '050-4234343'),
(4, 'Ankka Iines', '044-3434343');